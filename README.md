## 一、目标 
> 学习使用Yolov5识别tif图片**明细栏**和**表头**
> 
> 

## 二、数据预处理

### 2.1 数据转换（voc => yolo)

- **voc2yolo.py**
  - 转换：yolov5 使用的数据集是 yolo格式
  
### 2.2 模型适配

```cmd
python train.py --img 640 --batch 64--epochs 600 --data /yolov5-master/traindata/traindata.yaml --weights yolov5s.pt --nosave --cache
```